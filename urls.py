# from django.conf.urls import url
from django_prometheus import exports
from django.urls import path

urlpatterns = [
    path('metrics', exports.ExportToDjangoView, name="prometheus-django-metrics")
]