# Py App Django

Simples aplicativo web escrito em Python com *Framework* [Django](https://www.djangoproject.com/) para coleta de métricas com [Prometheus](https://prometheus.io/) utilizando biblioteca [django-prometheus](https://github.com/korfuri/django-prometheus).
