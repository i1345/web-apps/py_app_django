# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /src

RUN useradd --no-create-home -r -s /usr/sbin/nologin app && chown -R app /src

COPY --chown=app:app requirements.txt requirements.txt

RUN pip3 install --upgrade --user pip
RUN pip3 install -r requirements.txt

COPY --chown=app:app ./urls.py /usr/local/lib/python3.8/site-packages/django_prometheus/urls.py
COPY --chown=app:app ./webapp webapp

EXPOSE 8000

USER app

WORKDIR /src/webapp

RUN python3 manage.py collectstatic --noinput -c

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000", "--noreload"]
