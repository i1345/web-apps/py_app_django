from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index),
    path('about', views.view_about),
    path('blog-single', views.view_blog_single),
    path('blog', views.view_blog),
    path('contact', views.view_contact),
    path('services', views.view_services)
]