from django.shortcuts import render


def view_index(request):
    return render(
        request,
        'index.html'
    )


def view_about(request):
    return render(
        request,
        'about.html'
    )


def view_blog_single(request):
    return render(
        request,
        'blog-single.html'
    )


def view_blog(request):
    return render(
        request,
        'blog.html'
    )


def view_contact(request):
    return render(
        request,
        'contact.html'
    )


def view_services(request):
    return render(
        request,
        'services.html'
    )
